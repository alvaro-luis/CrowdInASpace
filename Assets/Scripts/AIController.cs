﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIController : MonoBehaviour
{

	public float sensorLenght = 5.0f;
	public float speed = 10.0f;
	public float directionValue = 1.0f;
	public float turnValue = 0.0f;
	public float turnSpeed = 5.0f;
	Collider myCollider;

	void Start ()
	{
		myCollider = GetComponent<Collider>();
	}


	void Update ()
	{
		RaycastHit hit;
		int flag= 0;

		//float sensorXAxis =  sensorLenght + transform.localScale.x;
		
		//Right Sensor
		if ( Physics.Raycast(transform.position, transform.right, out hit, sensorLenght + transform.localScale.x))
		{
			if (hit.collider.tag != "Obstacle" || hit.collider == myCollider)
			{
				return;
			}

			turnValue -= 1;
			flag++;
		}
		
		//Left Sensor
		if ( Physics.Raycast(transform.position, -transform.right, out hit, sensorLenght + transform.localScale.x))
		{
			if (hit.collider.tag != "Obstacle" || hit.collider == myCollider)
			{
				return;
			}

			turnValue += 1;
			flag++;
		}
		
		//float sensorYAxis =  sensorLenght + transform.localScale.z;
		//Front Sensor
		if ( Physics.Raycast(transform.position, transform.forward, out hit, sensorLenght + transform.localScale.z))
		{
			if (hit.collider.tag != "Obstacle" || hit.collider == myCollider)
			{
				return;
			}
			if (directionValue == 1.0f)
			{
				directionValue = -1;
			}
			flag++;
		}

		//Back Sensor
		if ( Physics.Raycast(transform.position, -transform.forward, out hit, sensorLenght + transform.localScale.z))
		{
			if (hit.collider.tag != "Obstacle" || hit.collider == myCollider)
			{
				return;
			}
			
			if (directionValue == -1.0f)
			{
				directionValue = 1;
			}
			flag++;
		}

		if (flag == 0)
		{
			turnValue = 0;
		}

		transform.Rotate(Vector3.up * (turnSpeed * turnValue) * Time.deltaTime);
		transform.position  += transform.forward * (speed * directionValue) * Time.deltaTime;
	}

	void OnDrawGizmos()
	{
		// Vector3 lookForward =  transform.forward * (sensorLenght + transform.localScale.z);
		// Vector3 lookRight =  transform.right * (sensorLenght + transform.localScale.x);
		// //Debug.Log("Seek: " + seek) ;
		// Gizmos.DrawRay(transform.position, lookForward);
		// Gizmos.DrawRay(transform.position, -lookForward);
		// Gizmos.DrawRay(transform.position, lookRight);
		// Gizmos.DrawRay(transform.position, -lookRight);

	}
}
